package com.Fawaz.Main;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.Fawaz.Mob.Player;
import com.Fawaz.Mob.Room;
import com.Fawaz.Packets.DisconnectPacket;
import com.Fawaz.Packets.HandsPacket;
import com.Fawaz.Packets.LoginPacket;
import com.Fawaz.Packets.LoginResponsePacket;
import com.Fawaz.Packets.PlayerInformation;
import com.Fawaz.Packets.PrivateRoomInformation;
import com.Fawaz.Packets.RoomRequestPacket;
import com.Fawaz.Packets.StartPacket;
import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import com.esotericsoftware.kryonet.Listener.ThreadedListener;
import com.esotericsoftware.kryonet.Server;
import com.esotericsoftware.minlog.Log;

public class RPSServer {

	Server server;
	private int version = 1;

	private ConcurrentLinkedQueue<Player> clientPool = new ConcurrentLinkedQueue<Player>();
	private ConcurrentHashMap<Connection, Player> clientMap = new ConcurrentHashMap<Connection, Player>();
	private ConcurrentLinkedQueue<Room> room3Queue = new ConcurrentLinkedQueue<Room>();
	private ConcurrentLinkedQueue<Room> room25Queue = new ConcurrentLinkedQueue<Room>();
	private ConcurrentLinkedQueue<Room> room5Queue = new ConcurrentLinkedQueue<Room>();
	private ConcurrentLinkedQueue<Room> room10Queue = new ConcurrentLinkedQueue<Room>();
	private ConcurrentLinkedQueue<Room> room100Queue = new ConcurrentLinkedQueue<Room>();
	private ConcurrentLinkedQueue<Room> roomPool = new ConcurrentLinkedQueue<Room>();
	private ConcurrentHashMap<Integer, Room> privateRooms = new ConcurrentHashMap<Integer, Room>(
			10, .9f, 4);
	private Random random = new Random();

	public RPSServer() {
		server = new Server();
		server.getKryo().register(LoginPacket.class);
		server.getKryo().register(LoginResponsePacket.class);
		server.getKryo().register(RoomRequestPacket.class);
		server.getKryo().register(PlayerInformation.class);
		server.getKryo().register(PlayerInformation[].class);
		server.getKryo().register(StartPacket.class);
		server.getKryo().register(HandsPacket.class);
		server.getKryo().register(DisconnectPacket.class);
		server.getKryo().register(PrivateRoomInformation.class);

		try {
			server.bind(new InetSocketAddress("23.239.17.61", 55556), null);
			// server.bind(new InetSocketAddress("192.168.8.107", 55556), null);
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public void init() {
		ExecutorService threadpool = Executors.newCachedThreadPool();
		server.addListener(new ServerListener(new Listener(), threadpool));
		server.start();
	}

	public static void main(String[] args) {
		RPSServer sc = new RPSServer();
		sc.init();

	}

	private void sendRoomInformation(Room room) {
		PlayerInformation rip;
		PlayerInformation[] roomInformationPacket = new PlayerInformation[room
				.getPlayers().size()];
		int i = 0;
		for (Player p : room.getPlayers()) {
			rip = new PlayerInformation();
			rip.name = p.getName();
			rip.id = p.getConnection().getID();
			roomInformationPacket[i++] = rip;
		}

		for (Player p : room.getPlayers())
			p.getConnection().sendTCP(roomInformationPacket);
	}

	private void sendStart(Room room) {
		StartPacket sp = new StartPacket();
		for (Player p : room.getPlayers()) {
			float startTime = 5000 - p.getConnection().getReturnTripTime() / 2;
			sp.startTime = startTime / 1000;
			room.setInGame(true);
			p.getConnection().sendTCP(sp);
		}

	}

	private Room getRoom(int rounds) {
		Room room;
		switch (rounds) {
		case 3:
			room = room3Queue.peek();
			if (room == null) {
				Log.debug("Room", "Room has to be created ");
				room = getFromPool();
				room3Queue.add(room);
			}
			return room;
		case 5:
			room = room5Queue.peek();
			if (room == null) {
				Log.debug("Room", "Room has to be created ");
				room = getFromPool();
				room5Queue.add(room);
			}
			return room;
		case 10:
			room = room10Queue.peek();
			if (room == null) {
				Log.debug("Room", "Room has to be created ");
				room = getFromPool();
				room10Queue.add(room);
			}
			return room;
		case 25:
			room = room25Queue.peek();
			if (room == null) {
				Log.debug("Room", "Room has to be created ");
				room = getFromPool();
				room25Queue.add(room);
			}
			return room;
		case 100:
			room = room100Queue.peek();
			if (room == null) {
				Log.debug("Room", "Room has to be created ");
				room = getFromPool();
				room100Queue.add(room);
			}
			room.setRounds(rounds);
			return room;
		}
		return null;
	}

	private Room getFromPool() {
		Room room = roomPool.poll();
		if (room == null)
			room = new Room();
		return room;
	}

	private void sendtoAllExceptTCP(Player player, Object object) {
		Room room = player.getRoom();
		if (room != null)
			for (Player p : room.getPlayers()) {
				if (!player.equals(p))
					p.getConnection().sendTCP(object);
			}
	}

	private int getPrivateRoom() {
		int roomId = random.nextInt((99999 - 10000) + 1) + 10000;
		Room room = getFromPool();
		Room posConflict = privateRooms.putIfAbsent(roomId, room);
		if (posConflict != null) {
			room.reset();
			return getPrivateRoom();
		} else {
			return roomId;
		}

	}

	private class ServerListener extends ThreadedListener {

		public ServerListener(Listener arg0, ExecutorService arg1) {
			super(arg0, arg1);

		}

		@Override
		public void connected(Connection connection) {
			connection.setTimeout(7000);
			connection.setKeepAliveTCP(2000);
		}

		@Override
		public void disconnected(Connection connection) {

			Player player = clientMap.remove(connection);
			if (player != null) {
				Room room = player.getRoom();
				if (room != null) {
					room.removePlayer(player);
					if (room.isInGame()) {
						DisconnectPacket dp = new DisconnectPacket();
						for (Player p : room.getPlayers())
							p.getConnection().sendTCP(dp);
						room.reset();
					} else
						sendRoomInformation(room);
					switch (room.getType()) {
					default:
						privateRooms.remove(room.getType());
						room.setInQueue(false);
					case 2:
						Log.debug("adding to the Pool");
						if (!room.isInQueue()) {
							room.setInQueue(true);
							room.reset();
							roomPool.add(room);
						}
						break;

					}
				}
				clientPool.add(player.reset());
			}

		}

		@Override
		public void received(Connection connection, Object object) {
			if (!object.getClass().getSimpleName().equals("KeepAlive"))
				Log.info("Recieved", object.getClass().getSimpleName());
			if (object instanceof LoginPacket) {
				LoginPacket lp = (LoginPacket) object;
				LoginResponsePacket lrp = new LoginResponsePacket();
				if (version == lp.version) {
					lrp.accepted = true;
					Player p = clientPool.poll();
					if (p == null)
						p = new Player();
					p.setConnection(connection);
					clientMap.put(connection, p);
				} else {
					lrp.accepted = false;
					lrp.message = "Wrong version \nPlease update your app";
				}
				Log.debug("Sennding login response packet");
				connection.sendTCP(lrp);

			}

			else if (object instanceof RoomRequestPacket) {
				RoomRequestPacket rrp = (RoomRequestPacket) object;
				PrivateRoomInformation pri;
				Player player = clientMap.get(connection);
				Room room = null;
				if (player != null) {
					player.setInitialInformation(rrp.name);
					switch (rrp.requestType) {
					case 2: // Public Room
						room = getRoom(rrp.rounds);
						if (room.addPlayer(player)) {
							switch (rrp.rounds) {
							case 3:
								room3Queue.remove(room);
								break;
							case 5:
								room5Queue.remove(room);
								break;
							case 10:
								room10Queue.remove(room);
								break;
							case 25:
								room25Queue.remove(room);
								break;
							case 100:
								room100Queue.remove(room);
								break;
							}
							room.setInQueue(false);
							sendRoomInformation(room);
							sendStart(room);
						} else
							sendRoomInformation(room);
						break;
					case 3: // Create a private
						pri = new PrivateRoomInformation();
						pri.id = getPrivateRoom();
						pri.rounds = rrp.rounds;
						room = privateRooms.get(pri.id);
						room.addPlayer(player);
						room.setInQueue(false);
						room.setType(pri.id);
						room.setRounds(rrp.rounds);
						sendRoomInformation(room);
						connection.sendTCP(pri);
						break;
					default: // Invited to a room
						room = privateRooms.get(rrp.requestType);
						if (room == null) {
							LoginResponsePacket lrp = new LoginResponsePacket();
							lrp.accepted = false;
							lrp.message = "No room with that id\ntry again";
							connection.sendTCP(lrp);
							return;
						} else {
							pri = new PrivateRoomInformation();
							pri.id = rrp.requestType;
							pri.rounds = room.getRounds();
							if (room.addPlayer(player)) {
								room.setInQueue(false);
								sendRoomInformation(room);
								connection.sendTCP(pri);
								sendStart(room);
							}
						}

					}
				}
			}

			else if (object instanceof HandsPacket) {
				Player player = clientMap.get(connection);
				if (player != null) {
					if (player.getRoom() != null)
						player.getRoom().setInGame(true);
					sendtoAllExceptTCP(player, object);
				}
			}

			else if (object instanceof DisconnectPacket) {
				Player player = clientMap.get(connection);
				if (player != null) {
					if (player.getRoom() != null)
						player.getRoom().removePlayer(player);
					connection.close();
					sendtoAllExceptTCP(player, object);
				}
			}
		}
	}

}
