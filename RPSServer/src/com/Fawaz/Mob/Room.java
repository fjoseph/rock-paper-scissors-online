package com.Fawaz.Mob;

import java.util.concurrent.ConcurrentLinkedQueue;

import com.esotericsoftware.minlog.Log;

public class Room {

	private ConcurrentLinkedQueue<Player> playersinRoom;
	private boolean inQueue;
	private boolean inGame = false;
	private int type;
	private int rounds;

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public int getRounds() {
		return rounds;
	}

	public void setRounds(int rounds) {
		this.rounds = rounds;
	}

	public Room() {
		playersinRoom = new ConcurrentLinkedQueue<Player>();
		inQueue = true;
		inGame = false;
	}

	public boolean addPlayer(Player player) {
		player.setRoom(this);
		playersinRoom.add(player);
		Log.debug("Room", "Size of room: " + playersinRoom.size());
		return isFull();
	}

	public boolean isFull() {
		return playersinRoom.size() == 2;
	}

	public Room reset() {
		playersinRoom.clear();
		inQueue = true;
		inGame = false;
		return this;
	}

	public ConcurrentLinkedQueue<Player> getPlayers() {
		return playersinRoom;
	}

	public boolean removePlayer(Player player) {
		playersinRoom.remove(player);
		return playersinRoom.size()==0;
	}

	public boolean isInQueue() {
		return inQueue;
	}

	public void setInQueue(boolean inQueue) {
		this.inQueue = inQueue;
	}

	public int size() {
		return playersinRoom.size();
	}

	public boolean isInGame() {
		return inGame;
	}

	public void setInGame(boolean inGame) {
		this.inGame = inGame;
	}


}
