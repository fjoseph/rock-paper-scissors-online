package com.Fawaz.Mob;

import com.esotericsoftware.kryonet.Connection;

public class Player {

	private Connection connection;
	private String name;
	private Room room = null;
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Connection getConnection() {
		return connection;
	}

	public void setConnection(Connection connection) {
		this.connection = connection;
	}


	public void setInitialInformation(String name) {
		this.name = name;
	}

	public Room getRoom() {
		return room;
	}
	
	public void setRoom(Room room) {
		this.room = room;
	}
	public Player reset() {
		connection = null;
		room = null;
		name = null;
		return this;
	}

}
