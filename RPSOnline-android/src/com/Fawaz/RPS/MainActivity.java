package com.Fawaz.RPS;

import java.io.IOException;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;

import android.content.Intent;
import android.os.Bundle;

import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import com.google.android.gms.auth.GoogleAuthException;
import com.google.android.gms.auth.GoogleAuthUtil;
import com.google.android.gms.auth.UserRecoverableAuthException;
import com.google.example.games.basegameutils.GameHelper;
import com.google.example.games.basegameutils.GameHelper.GameHelperListener;
import com.jirbo.adcolony.AdColony;
import com.jirbo.adcolony.AdColonyVideoAd;

public class MainActivity extends AndroidApplication implements
		GameHelperListener, ThirdPartyInteface {

	private GameHelper aHelper;
	private RPSOnline game;
	private String account, scope;
	final static String APP_ID = "app1d7aa63be62e4f0e85";
	final static String ZONE_ID = "vzb2e2f2990c7d46fc95";

	public MainActivity() {
		aHelper = new GameHelper(this);

	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		AdColony.configure(this, "version:1.2.3,store:google", APP_ID, ZONE_ID);
		AndroidApplicationConfiguration cfg = new AndroidApplicationConfiguration();
		cfg.useGL20 = false;
		aHelper.setup(this);
		game = new RPSOnline(this);
		initialize(game, cfg);
	}

	@Override
	public void onStart() {
		super.onStart();
		aHelper.onStart(this);

	}

	@Override
	public void onStop() {
		super.onStop();
		aHelper.onStop();
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		AdColony.pause();
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		AdColony.resume(this);
	}

	@Override
	public void onActivityResult(int request, int response, Intent data) {
		super.onActivityResult(request, response, data);
		aHelper.onActivityResult(request, response, data);
	}

	public void onSignInFailed() {
		System.out.println("sign in failed");
	}

	public void onSignInSucceeded() {
		System.out.println("sign in succeeded");
	}

	public void Login() {
		try {
			runOnUiThread(new Runnable() {

				// @Override
				public void run() {
					aHelper.beginUserInitiatedSignIn();
				}
			});
		} catch (final Exception ex) {

		}
	}

	public void LogOut() {
		try {
			runOnUiThread(new Runnable() {

				// @Override
				public void run() {
					aHelper.signOut();
				}
			});
		} catch (final Exception ex) {

		}

	}

	public boolean getSignedIn() {
		return aHelper.isSignedIn();
	}

	public void submitScore(int rounds, int _score) {
		String leaderboardID = null;

		switch (rounds) {
		case 3:
			leaderboardID = getString(R.string.leaderboard_best_of_3);
			break;
		case 5:
			leaderboardID = getString(R.string.leaderboard_best_of_5);
			break;
		case 10:
			leaderboardID = getString(R.string.leaderboard_best_of_10);
			break;
		case 25:
			leaderboardID = getString(R.string.leaderboard_best_of_25);
			break;
		case 100:
			leaderboardID = getString(R.string.leaderboard_best_of_100);
			break;
		}
		System.out.println("in submit score");
		aHelper.getGamesClient().submitScore(leaderboardID, _score);
	}

	@Override
	public void showLeaderBoards() {
		startActivityForResult(aHelper.getGamesClient()
				.getAllLeaderboardsIntent(), 105);
	}

	@Override
	public String getName() {
		return aHelper.getGamesClient().getCurrentPlayer().getDisplayName();
	}

	@Override
	public void displayAchievements() {
		startActivityForResult(
				aHelper.getGamesClient().getAchievementsIntent(), 500);
	}

	@Override
	public void unlockAchievements(int index) {
		String achievementId = null;
		switch (index) {
		case 1:
			achievementId = getString(R.string.achievement_win_a_game);
			break;
		case 2:
			achievementId = getString(R.string.achievement_practice_makes_perfect);
			break;
		case 3:
			achievementId = getString(R.string.achievement_rewarded_faith);
			break;
		case 4:
			achievementId = getString(R.string.achievement_win_a_best_of_3);
			break;
		case 5:
			achievementId = getString(R.string.achievement_win_a_best_of_5);
			break;
		case 6:
			achievementId = getString(R.string.achievement_win_a_best_of_10);
			break;
		case 7:
			achievementId = getString(R.string.achievement_win_a_best_of_25);
			break;
		case 8:
			achievementId = getString(R.string.achievement_win_a_best_of_100);
			break;
		case 9:
			achievementId = getString(R.string.achievement_usurper);
			break;
		case 10:
			achievementId = getString(R.string.achievement_david_v_goliath);
			break;
		case 11:
			achievementId = getString(R.string.achievement_double);
			break;
		case 12:
			achievementId = getString(R.string.achievement_triple);
			break;
		case 13:
			achievementId = getString(R.string.achievement_quadra);
			break;
		case 14:
			achievementId = getString(R.string.achievement_penta);
			break;
		case 15:
			achievementId = getString(R.string.achievement_participation_trophy);
			break;
		case 16:
			achievementId = getString(R.string.achievement_marathon_practice);
			break;
		case 17:
			achievementId = getString(R.string.achievement_marathon);
			break;
		case 18:
			achievementId = getString(R.string.achievement_neophyte);
			break;
		case 19:
			achievementId = getString(R.string.achievement_apprentice);
			break;
		case 20:
			achievementId = getString(R.string.achievement_journeyman);
			break;
		case 21:
			achievementId = getString(R.string.achievement_veteran);
			break;
		case 22:
			achievementId = getString(R.string.achievement_master);
			break;
		case 23:
			achievementId = getString(R.string.achievement_a_rivalry_beginning);
			break;
		case 24:
			achievementId = getString(R.string.achievement_you_are_cordially_invited);
			break;
		}

		aHelper.getGamesClient().unlockAchievement(achievementId);
	}

	public void resetAchievements() {
		Runnable runnable = new Runnable() {

			@Override
			public void run() {
				account = aHelper.getGamesClient().getCurrentAccountName();
				scope = aHelper.getScopes();
				String token = "f";
				try {
					token = GoogleAuthUtil.getToken(MainActivity.this, account,
							scope);
				} catch (UserRecoverableAuthException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (GoogleAuthException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				String[] ids = getResources().getStringArray(R.array.idsReset);

				HttpClient client = new DefaultHttpClient();
				for (String id : ids) {
					HttpPost post = new HttpPost("https://www.googleapis.com"
							+ "/games/v1management" + "/achievements/" + id
							+ "/reset" + "?access_token=" + token);

					try {
						client.execute(post);
					} catch (ClientProtocolException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		};

		new Thread(runnable).start();

	}

	@Override
	public void showAd() {
		AdColonyVideoAd ad = new AdColonyVideoAd();
		ad.show();
	}

	@Override
	public void incrementWinCountAchievement() {
		String achievementId;
		achievementId = getString(R.string.achievement_neophyte);
		aHelper.getGamesClient().incrementAchievement(achievementId, 1);
		achievementId = getString(R.string.achievement_apprentice);
		aHelper.getGamesClient().incrementAchievement(achievementId, 1);
		achievementId = getString(R.string.achievement_journeyman);
		aHelper.getGamesClient().incrementAchievement(achievementId, 1);
		achievementId = getString(R.string.achievement_veteran);
		aHelper.getGamesClient().incrementAchievement(achievementId, 1);
		achievementId = getString(R.string.achievement_master);
		aHelper.getGamesClient().incrementAchievement(achievementId, 1);
	}
}