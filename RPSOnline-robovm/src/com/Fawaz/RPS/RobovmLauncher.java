package com.Fawaz.RPS;

import java.util.ArrayList;

import org.robovm.bindings.adcolony.AdColony;
import org.robovm.bindings.adcolony.AdColonyAdDelegate;
import org.robovm.bindings.adcolony.AdColonyDelegate;
import org.robovm.bindings.cocoatouch.gamekit.GKAchievement;
import org.robovm.bindings.cocoatouch.gamekit.GKLeaderboard;
import org.robovm.bindings.gamecenter.GameCenterListener;
import org.robovm.bindings.gamecenter.GameCenterManager;
import org.robovm.cocoatouch.foundation.NSArray;
import org.robovm.cocoatouch.foundation.NSAutoreleasePool;
import org.robovm.cocoatouch.foundation.NSString;
import org.robovm.cocoatouch.uikit.UIApplication;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.backends.iosrobovm.IOSApplication;
import com.badlogic.gdx.backends.iosrobovm.IOSApplicationConfiguration;

public class RobovmLauncher extends IOSApplication.Delegate implements
		ThirdPartyInteface,AdColonyDelegate, AdColonyAdDelegate  {

	private IOSApplication iosApp;
	private GameCenterManager gcManager;
	private boolean signedIn = false;
	private Preferences pref;
	final static String APP_ID = "appf81e752369cc44069e";
	final static String ZONE_ID = "vz370ad709c9474699b1";

	@Override
	protected IOSApplication createApplication() {
		IOSApplicationConfiguration config = new IOSApplicationConfiguration();
		config.orientationLandscape = true;
		config.orientationPortrait = false;
		ArrayList<NSString> aZones = new ArrayList<NSString>();
		aZones.add(new NSString(ZONE_ID));
		NSArray <NSString> zones = new NSArray<NSString> (aZones);
		AdColony.configure(APP_ID, zones, this, true);
		iosApp = new IOSApplication(new RPSOnline(this), config);
		return iosApp;
	}

	public static void main(String[] argv) {
		NSAutoreleasePool pool = new NSAutoreleasePool();
		UIApplication.main(argv, null, RobovmLauncher.class);
		pool.drain();
	}

	@Override
	public void Login() {
		gcManager = new GameCenterManager(UIApplication.getSharedApplication()
				.getKeyWindow(), new GameCenterListener() {
			@Override
			public void playerLoginFailed() {
				System.out.println("playerLoginFailed");
				signedIn = false;
			}

			@Override
			public void playerLoginCompleted() {
				System.out.println("playerLoginCompleted");
				signedIn = true;
			}

			@Override
			public void achievementReportCompleted() {
				System.out.println("achievementReportCompleted");
			}

			@Override
			public void achievementReportFailed() {
				System.out.println("achievementReportFailed");
			}

			@Override
			public void achievementsLoadCompleted(
					ArrayList<GKAchievement> achievements) {
				System.out.println("achievementsLoadCompleted: "
						+ achievements.size());
			}

			@Override
			public void achievementsLoadFailed() {
				System.out.println("achievementsLoadFailed");
			}

			@Override
			public void achievementsResetCompleted() {
				System.out.println("achievementsResetCompleted");
			}

			@Override
			public void achievementsResetFailed() {
				System.out.println("achievementsResetFailed");
			}

			@Override
			public void scoreReportCompleted() {
				System.out.println("scoreReportCompleted");
			}

			@Override
			public void scoreReportFailed() {
				System.out.println("scoreReportFailed");
			}

			@Override
			public void leaderboardsLoadCompleted(
					ArrayList<GKLeaderboard> scores) {
				System.out.println("scoresLoadCompleted: " + scores.size());
			}

			@Override
			public void leaderboardsLoadFailed() {
				System.out.println("scoresLoadFailed");
			}
		});
		gcManager.login();
		
	}

	@Override
	public boolean getSignedIn() {
		// TODO Auto-generated method stub
		return signedIn;
	}

	@Override
	public void submitScore(int rounds, int score) {
		// TODO Auto-generated method stub
		String identifier = "rpslead" + rounds;
		gcManager.reportScore(identifier, score);
	}

	@Override
	public void showLeaderBoards() {
		// TODO Auto-generated method stub
		gcManager.showLeaderboardsView();
	}

	@Override
	public void displayAchievements() {
		// TODO Auto-generated method stub
		gcManager.showAchievementsView();
	}

	@Override
	public void unlockAchievements(int index) {
		// TODO Auto-generated method stub
		String id = "rps" + --index;
		gcManager.reportAchievement(id);

	}

	@Override
	public void incrementWinCountAchievement() {
		// TODO Auto-generated method stub
		String identifier;
		float achievementGoal = 3;
		float win = pref.getInteger("timesWon", 0);
		identifier = "rps17";
		gcManager
				.reportAchievement(identifier, ((win / achievementGoal) * 100));
		achievementGoal = 5;
		identifier = "rps18";
		gcManager
				.reportAchievement(identifier, ((win / achievementGoal) * 100));
		achievementGoal = 10;
		identifier = "rps19";
		gcManager
				.reportAchievement(identifier, ((win / achievementGoal) * 100));
		achievementGoal = 25;
		identifier = "rps20";
		gcManager
				.reportAchievement(identifier, ((win / achievementGoal) * 100));
		achievementGoal = 100;
		identifier = "rps21";
		gcManager
				.reportAchievement(identifier, ((win / achievementGoal) * 100));
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		pref = Gdx.app.getPreferences("RPSOnline");
		return pref.getString("name", "null");
	}

	@Override
	public void showAd() {
		// TODO Auto-generated method stub
		AdColony.playVideoAd(ZONE_ID, this);
	}

	@Override
	public void onAdColonyAdStartedInZone(String zoneID) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onAdColonyAdAttemptFinished(boolean shown, String zoneID) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onAdColonyAdAvailabilityChange(boolean available, String zoneID) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onAdColonyV4VCReward(boolean success, String currencyName,
			int amount, String zoneID) {
		// TODO Auto-generated method stub
		
	}

}
