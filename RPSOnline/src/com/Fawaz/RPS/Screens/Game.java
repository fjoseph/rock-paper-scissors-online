package com.Fawaz.RPS.Screens;

import com.Fawaz.RPS.RPSOnline;
import com.Fawaz.RPS.Utils.GameLogic;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.scenes.scene2d.Stage;

public class Game implements Screen {

	Stage stage;
	RPSOnline game;
	GameLogic gl;

	public Game(int modeCode, int rounds, RPSOnline game) {
		// modeCode 1 cpu/practice 2 findPublicRoom
		// 3 create PrivateRoom
		// anything else private Room Code
		// modeCode
		this.game = game;
		stage = new Stage(640, 640);
		stage.addActor(game.getVolume());
		Gdx.input.setInputProcessor(stage);
		gl = new GameLogic(modeCode, rounds, stage, game);
	}

	@Override
	public void render(float delta) {
		// TODO Auto-generated method stub
		Gdx.gl.glClearColor(1, 1, 1, 1);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		gl.render(delta);
	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub

	}

	@Override
	public void show() {
		// TODO Auto-generated method stub

	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub

	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub

	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub

	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		stage.dispose();

	}

}
