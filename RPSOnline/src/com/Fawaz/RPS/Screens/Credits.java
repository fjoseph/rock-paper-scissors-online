package com.Fawaz.RPS.Screens;

import com.Fawaz.RPS.RPSOnline;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.Application.ApplicationType;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;

public class Credits implements Screen {

	private RPSOnline game;
	private Stage stage;

	public Credits(RPSOnline g) {
		this.game = g;

		stage = new Stage(640, 640);
		LabelStyle ls = new LabelStyle(game.getFont(), Color.BLACK);
		Label back = new Label("Back", ls);
		Label credits = new Label("", ls);
		ScrollPane sp = new ScrollPane(credits);

		sp.setBounds(0, 65, 640, 575);
		back.setPosition(30, 23);
		Gdx.input.setInputProcessor(stage);

		back.addListener(new InputListener() {
			@Override
			public boolean touchDown(InputEvent event, float x, float y,
					int pointer, int button) {
				game.setScreen(new MainMenu(game));
				return true;
			}
		});

		stage.addActor(sp);
		stage.addActor(back);

		String text = "Thanks \n "
				+ "David Abimbola -- Being an awesome friend again\n"
				+ " \nSpecial thanks"
				+ "\nLibgdx and the team behind it,Thanks for building the framework behind this game"
				+ "\nhttp://libgdx.badlogicgames.com/ "
				+ gcNiklas()
				+ "\nArt resources made by the skilled artists at AudibleDesign.net"
				+ "\n\nThis game \"Rock Paper Scissors Multiplayer\" uses these sounds from freesound:"
				+ "\nError.wav by Autistic Lucario (http://www.freesound.org/people/Autistic%20Lucario/)"
				+ "\nscary.mp3 by Kastenfrosch (http://www.freesound.org/people/Kastenfrosch/)"
				+ "\nNodens (Field Song) by axtoncrolley (http://www.freesound.org/people/axtoncrolley/)"
				+ "\nApplause 3 by unfa (http://www.freesound.org/people/unfa/)"
				+ "\nShocked gasp.wav by GentlemanWalrus (http://www.freesound.org/people/GentlemanWalrus/)"
				+ "\nscanner beep.wav by kalisemorrison (http://www.freesound.org/people/kalisemorrison/)"
				+ "\nsynth-cricket.wav by guitarguy1985 (http://www.freesound.org/people/guitarguy1985/)"
				+ "\nSad Trombone.wav by Benboncan (http://www.freesound.org/people/Benboncan/)"
				+ "\nlevel_up_3note2.wav by jivatma07 (http://www.freesound.org/people/jivatma07/)"
				+ "\n\nIcons created by Android Asset Studio is licensed under a Creative Commons Attribution 3.0 Unported License."
				+ "\nhttp://android-ui-utils.googlecode.com/hg/asset-studio/dist/index.html"
				+ "\n\n Cabin Sketch Bold font - Created in 2011 by Pablo Impallari";
		;

		credits.setText(text);
		credits.setWrap(true);
	}

	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(1, 1, 1, 1);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		stage.act();
		stage.draw();
	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub

	}

	@Override
	public void show() {
		// TODO Auto-generated method stub

	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub

	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub

	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub

	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		stage.dispose();
	}

	public String gcNiklas() {
		if (Gdx.app.getType() != ApplicationType.iOS)
			return "";
		else
			return "\niOS version made possible by Niklas Therning and theRobovm Team -- robovm.org"
					+ "\nAdColony custom bindings by HD_92 on badlogic forums"
					+ "\nGame Center custom bindings by HD_92 and YaW on badlogic forums";
	}

}
