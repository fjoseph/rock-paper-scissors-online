package com.Fawaz.RPS.Screens;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.alpha;

import com.Fawaz.RPS.RPSOnline;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.Application.ApplicationType;
import com.badlogic.gdx.Input.TextInputListener;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;

public class MainMenu implements Screen {

	RPSOnline game;
	Stage stage;
	private Label cpu, private_room, public_room, best3, best5, best25, best10,
			best100, leaderBoard, showAchievement, credits, findRoom;
	private Image r, p, s;
	private Label online, back, textfield;
	private int modeCode;
	private String[] badWordList = { "anal", "anus", "arrse", "arse", "ass",
			"fuck", "assfukka", "balls", "bastard", "beastial", "bellend",
			"bestial", "bestiality", "biatch", "bitch", "bloody", "blowjob",
			"boiolas", "bollock", "bollok", "boner", "boob", "booobs",
			"boooobs", "booooobs", "booooooobs", "breast", "buceta", "bugger",
			"bum", "butt", "cawk", "chink", "cipa", "cl1t", "clit", "cnut",
			"cock", "cok", "coon", "cox", "crap", "cum", "cunilingus",
			"cunillingus", "cunnilingus", "cunt", "cyalis", "damn", "dick",
			"dildo", "dink", "dirsa", "dlck", "doggin", "donkeyribber",
			"doosh", "duche", "dyke", "ejaculat", "ejakulate", "fag", "fanny",
			"fatass", "fcuk", "fecker", "felching", "fellate", "fellatio",
			"flange", "fook", "fudgepacker", "fuk", "fux", "fuxor", "gangbang",
			"goatse", "goddam", "sex", "heshe", "hoar", "hoare", "homo",
			"horniest", "horny", "jackoff", "jerkoff", "jism", "jiz", "jizm",
			"kawk", "knob", "kock", "kondum", "kum", "kunilingus", "lust",
			"masochist", "masterbate", "masterbation", "masturbate", "mofo",
			"mothafuck", "muff", "mutha", "muther", "mutherfucker", "nazi",
			"nigga", "nigger", "nob", "numbnuts", "nutsack", "orgasim",
			"orgasm", "pecker", "penis", "phonesex", "phuck", "phuk", "phuq",
			"pigfucker", "piss", "poop", "porn", "prick", "pron", "pube",
			"pusse", "pussi", "pussies", "pussy", "rectum", "retard", "rimjaw",
			"rimming", "shit", "sadist", "schlong", "scroat", "scrote",
			"scrotum", "semen", "shag", "shemale", "shit", "skank", "slut",
			"sluts", "smegma", "smut", "snatch", "sonofabitch", "spac",
			"spunk", "teets", "teez", "testical", "testicle", "tit", "tosser",
			"twat", "twunt", "twunter", "vagina", "vulva", "w00se", "wang",
			"wank", "whoar", "whore", "willies", "willy"

	};
	private Preferences prefs;

	public MainMenu(RPSOnline game) {
		this.game = game;
		stage = new Stage(640, 640);
		LabelStyle ls = new LabelStyle(game.getFont(), Color.BLACK);

		// First rounds of labels
		cpu = new Label("Play with Cpu", ls);
		private_room = new Label("Create a private Room", ls);
		public_room = new Label("Play with random people", ls);
		credits = new Label("Credits", ls);
		findRoom = new Label("Tap to  enter room code", ls);

		// GooglePlay
		leaderBoard = new Label("LeaderBoards", ls);
		showAchievement = new Label("Achievements", ls);

		// Second rounds
		best3 = new Label("Best of 3", ls);
		best5 = new Label("Best of 5", ls);
		best10 = new Label("Best of 10", ls);
		best25 = new Label("Best of 25", ls);
		best100 = new Label("Best of 100", ls);
		online = new Label("Multiplayer", ls);
		back = new Label("Back", ls);

		// Listeners
		cpu.addListener(new ModeListener(1));
		private_room.addListener(new ModeListener(3));
		public_room.addListener(new ModeListener(2));
		best10.addListener(new RoundsListener(10));
		best100.addListener(new RoundsListener(100));
		best3.addListener(new RoundsListener(3));
		best5.addListener(new RoundsListener(5));
		best25.addListener(new RoundsListener(25));
		back.addListener(new InputListener() {
			public boolean touchDown(InputEvent event, float x, float y,
					int pointer, int button) {
				best25.remove();
				best3.remove();
				best5.remove();
				best10.remove();
				best100.remove();
				back.remove();
				stage.addActor(cpu);
				stage.addActor(private_room);
				stage.addActor(findRoom);
				stage.addActor(public_room);
				stage.addActor(credits);
				return true;
			}
		});
		showAchievement.addListener(new InputListener() {
			public boolean touchDown(InputEvent event, float x, float y,
					int pointer, int button) {
				MainMenu.this.game.displayAchievements();
				return true;
			}
		});
		credits.addListener(new InputListener() {
			public boolean touchDown(InputEvent event, float x, float y,
					int pointer, int button) {
				MainMenu.this.game.setScreen(new Credits(MainMenu.this.game));
				return true;
			}
		});
		findRoom.addListener(new InputListener() {
			public boolean touchDown(InputEvent event, float x, float y,
					int pointer, int button) {
				getCode();
				return true;
			}
		});
		leaderBoard.addListener(new InputListener() {
			public boolean touchDown(InputEvent event, float x, float y,
					int pointer, int button) {
				MainMenu.this.game.displayLeaderBoard();
				return true;
			}
		});

		// Assigning the three icons
		r = game.getImage(0);
		r.setPosition(70, 410);
		p = game.getImage(1);
		p.setPosition(270, 550);
		s = game.getImage(2);
		s.setPosition(470, 410);
		r.setVisible(true);
		p.setVisible(true);
		s.setVisible(true);

		// Setting position
		cpu.setPosition(320 - (.5f * cpu.getPrefWidth()), 320);
		private_room
				.setPosition(320 - (.5f * private_room.getPrefWidth()), 180);
		public_room.setPosition(320 - (.5f * public_room.getPrefWidth()), 250);
		findRoom.setPosition(320 - (.5f * findRoom.getPrefWidth()), 110);

		best3.setPosition(320 - (.5f * best3.getPrefWidth()), 320);
		best5.setPosition(320 - (.5f * best5.getPrefWidth()), 250);
		best10.setPosition(320 - (.5f * best10.getPrefWidth()), 180);
		best25.setPosition(320 - (.5f * best25.getPrefWidth()), 110);
		best100.setPosition(320 - (.5f * best100.getPrefWidth()), 40);
		online.setPosition(320 - (.5f * online.getPrefWidth()), 425);
		credits.setPosition(320 - (.5f * credits.getPrefWidth()), 40);
		back.setPosition(30, 23);

		// Google Play position
		showAchievement
				.setPosition(610 - (showAchievement.getPrefWidth()), 600);
		leaderBoard.setPosition(30, 600);

		cpu.addAction(alpha(0));
		findRoom.addAction(alpha(0));
		private_room.addAction(alpha(0));
		public_room.addAction(alpha(0));

		cpu.addAction(alpha(1, 1));
		findRoom.addAction(alpha(1, 1));
		private_room.addAction(alpha(1, 1));
		public_room.addAction(alpha(1, 1));

		Gdx.input.setInputProcessor(stage);
		stage.addActor(r);
		stage.addActor(p);
		stage.addActor(s);
		stage.addActor(online);
		stage.addActor(cpu);
		stage.addActor(game.getVolume());
		stage.addActor(private_room);
		stage.addActor(public_room);
		stage.addActor(findRoom);
		stage.addActor(leaderBoard);
		stage.addActor(showAchievement);
		stage.addActor(credits);
		// iOS only Code
		if (Gdx.app.getType() == ApplicationType.iOS) {
			prefs = Gdx.app.getPreferences("RPSOnline");
			textfield = new Label("Tap to enter your name", ls);
			if (!prefs.getString("name", "null").equals("null"))
				textfield.setText(prefs.getString("name", "null"));
			textfield.addListener(new InputListener() {
				public boolean touchDown(InputEvent event, float x, float y,
						int pointer, int button) {
					notAndroidGetName();
					return true;
				}

			});
			textfield.setPosition(320 - (.5f * textfield.getPrefWidth()), 490);
			stage.addActor(textfield);
		}

	}

	private void notAndroidGetName() {

		Gdx.input.getTextInput(new TextInputListener() {

			@Override
			public void input(String text) {
				char[] array = text.toCharArray();
				for (char c : array) {
					if (!Character.isLetter(c)) {
						textfield.setText("No symbols");
						textfield.setPosition(
								320 - (.5f * textfield.getPrefWidth()), 440);
						stage.setKeyboardFocus(null);
						return;
					}
				}
				if (!text.isEmpty())
					if (!checkString(text)) {
						textfield.setText("NO BAD WORDS");
						textfield.setPosition(
								320 - (.5f * textfield.getPrefWidth()), 490);
						stage.setKeyboardFocus(null);
					} else if (text.length() > 12) {
						textfield.setText("12 letter limit");
						textfield.setPosition(
								320 - (.5f * textfield.getPrefWidth()), 490);
					} else {
						prefs.putString("name", text);
						textfield.setText(text);
						textfield.setPosition(
								320 - (.5f * textfield.getPrefWidth()), 490);
						prefs.flush();
						stage.setKeyboardFocus(null);
					}

			}

			@Override
			public void canceled() {
				stage.setKeyboardFocus(null);
			}
		}, "Name", "");

	}

	private boolean checkString(String word) {
		for (int i = 0; i < badWordList.length; i++) {
			if (word.toLowerCase().contains(badWordList[i]))
				return false;
		}
		return true;

	}

	@Override
	public void render(float delta) {
		// TODO Auto-generated method stub
		Gdx.gl.glClearColor(1, 1, 1, 1);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		stage.act();
		stage.draw();

	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub

	}

	@Override
	public void show() {
		// TODO Auto-generated method stub
		game.playMusic(0);
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub

	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub

	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub

	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		stage.dispose();
	}

	private void getCode() {
		Gdx.input.getTextInput(new TextInputListener() {

			@Override
			public void input(String text) {
				int num;
				try {
					num = Integer.parseInt(text);
				} catch (NumberFormatException e) {
					findRoom.setText("Invalid room code");
					findRoom.setPosition(320 - (.5f * findRoom.getPrefWidth()),
							110);
					return;
				}

				game.setScreen(new Game(num, 1, game));

			}

			@Override
			public void canceled() {
				findRoom.setText("Tap to enter room code");
				findRoom.setPosition(320 - (.5f * findRoom.getPrefWidth()), 110);

			}
		}, "Room code", "");
	}

	private class ModeListener extends InputListener {

		private int i;

		public ModeListener(int i) {
			this.i = i;
		}

		@Override
		public boolean touchDown(InputEvent event, float x, float y,
				int pointer, int button) {
			modeCode = i;
			cpu.remove();
			private_room.remove();
			findRoom.remove();
			public_room.remove();
			credits.remove();
			stage.addActor(best25);
			stage.addActor(best3);
			stage.addActor(best10);
			stage.addActor(best5);
			stage.addActor(best100);
			stage.addActor(back);

			return true;
		}
	}

	private class RoundsListener extends InputListener {
		private int rounds;

		public RoundsListener(int rounds) {
			this.rounds = rounds;
		}

		@Override
		public boolean touchDown(InputEvent event, float x, float y,
				int pointer, int button) {
			game.stopMusic(0);
			if (modeCode == 1) {
				game.unlockAchievement(2);
				if (rounds == 100)
					game.unlockAchievement(16);
			}
			game.setScreen(new Game(modeCode, rounds, game));
			stage.dispose();

			return true;
		}
	}

}
