package com.Fawaz.RPS.Screens;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.delay;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.moveTo;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.run;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.sequence;

import com.Fawaz.RPS.RPSOnline;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;

public class SplashScreen extends InputListener implements Screen {
	private RPSOnline game;
	private Image r, p, s;
	private Label online;
	private Stage stage;

	public SplashScreen(final RPSOnline game) {
		this.game = game;
		stage = new Stage(640, 640);
		Gdx.input.setInputProcessor(stage);
		stage.addListener(this);
		r = game.getImage(0);
		p = game.getImage(1);
		s = game.getImage(2);
		r.setBounds(-200, 360, 100, 100);
		p.setBounds(270, 800, 100, 100);
		s.setBounds(800, 360, 100, 100);

		LabelStyle ls = new LabelStyle(game.getFont(), Color.BLACK);
		online = new Label("Multiplayer", ls);
		online.setPosition(320 - (.5f * online.getPrefWidth()), -100);
		stage.addActor(r);
		stage.addActor(p);
		stage.addActor(s);
		stage.addActor(online);

		r.addAction(moveTo(70, 410, .5f));

		p.addAction(delay(.5f, moveTo(270, 550, .5f)));

		s.addAction(delay(1, moveTo(470, 410, .5f)));

		Runnable transToMainMenu = new Runnable() {

			@Override
			public void run() {
				game.setScreen(new MainMenu(game));

			}
		};

		online.addAction(sequence(
				delay(1.5f, moveTo(320 - (.5f * online.getPrefWidth()), 425, 1)),
				run(transToMainMenu)));

	}

	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(1, 1, 1, 1);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		stage.act();
		stage.draw();
	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub

	}

	@Override
	public void show() {
		// TODO Auto-generated method stub

	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub

	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub

	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub

	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		stage.clear();
		stage.dispose();

	}

	@Override
	public boolean touchDown(InputEvent event, float x, float y, int pointer,
			int button) {
		r.setPosition(70, 410);
		p.setPosition(270, 550);
		s.setPosition(470, 410);
		r.clearActions();
		p.clearActions();
		s.clearActions();
		game.setScreen(new MainMenu(game));

		return true;
	}
}
