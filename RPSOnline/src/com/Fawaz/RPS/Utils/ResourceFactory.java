package com.Fawaz.RPS.Utils;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton.ImageButtonStyle;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Array;

public class ResourceFactory {

	private Array<Image> hands;
	private TextureAtlas textureAtlas;
	private ImageButton volume;
	private Array<Sound> soundArray;
	private Array<AtlasRegion> arrayRegion;
	private BitmapFont font;
	private Music music, winMusic;
	private boolean mainMenu;

	public ResourceFactory() {
		hands = new Array<Image>(3);
		soundArray = new Array<Sound>(4);
		textureAtlas = new TextureAtlas(
				Gdx.files.internal("data/RPSAssets.pack"));
		arrayRegion = textureAtlas.getRegions();
		mainMenu = false;

		soundArray.add(Gdx.audio.newSound(Gdx.files
				.internal("data/142608__autistic-lucario__error.wav")));
		soundArray.add(Gdx.audio.newSound(Gdx.files
				.internal("data/162472__kastenfrosch__scary.wav")));
		soundArray.add(Gdx.audio.newSound(Gdx.files
				.internal("data/76234__jivatma07__level-up-3note2.wav")));
		soundArray.add(Gdx.audio.newSound(Gdx.files
				.internal("data/202530__kalisemorrison__scanner-beep.wav")));
		soundArray.add(Gdx.audio.newSound(Gdx.files
				.internal("data/180005__gentlemanwalrus__shocked-gasp.wav")));
		soundArray.add(Gdx.audio.newSound(Gdx.files
				.internal("data/73581__benboncan__sad-trombone.wav")));
		soundArray.add(Gdx.audio.newSound(Gdx.files
				.internal("data/69439__guitarguy1985__synth-cricket.wav")));
		music = Gdx.audio.newMusic(Gdx.files
				.internal("data/172707__axtoncrolley__nodens-field-song.wav"));
		music.setLooping(true);
		winMusic = Gdx.audio.newMusic(Gdx.files
				.internal("data/162762_unfa_applause-3.wav"));

		hands.add(new Image(textureAtlas.findRegion("leftrockicon")));
		hands.add(new Image(textureAtlas.findRegion("leftpapericon")));
		hands.add(new Image(textureAtlas.findRegion("leftscissorsicon")));
		AtlasRegion trdup = textureAtlas.findRegion("volumeon");
		AtlasRegion trddown = textureAtlas.findRegion("volumeoff");
		ImageButtonStyle ibs = new ImageButtonStyle(null, null, null,
				new TextureRegionDrawable(trdup), null,
				new TextureRegionDrawable(trddown));
		volume = new ImageButton(ibs);
		volume.setBounds(565, 15, 70, 70);
		volume.addListener(new InputListener() {
			@Override
			public boolean touchDown(InputEvent event, float x, float y,
					int pointer, int button) {
				// TODO Auto-generated method stub
				if (!volume.isChecked()) {
					music.stop();
					winMusic.stop();
				} else if (volume.isChecked() && mainMenu == true)
					music.play();
				return true;
			}
		});

		Texture fontTexture = new Texture(
				Gdx.files.internal("data/CabinSketch_0.png"));
		fontTexture.setFilter(TextureFilter.Linear, TextureFilter.Linear);
		font = new BitmapFont(Gdx.files.internal("data/CabinSketch.fnt"),
				new TextureRegion(fontTexture));
	}

	public void dispose() {
		textureAtlas.dispose();
		font.dispose();
		for (Sound s : soundArray)
			s.dispose();
		music.dispose();
		winMusic.dispose();
	}

	public Image getImage(int index) {
		Image temp = hands.get(index);
		temp.clearListeners();
		return temp;
	}

	public BitmapFont getFont() {
		return font;
	}

	public void playSound(int index) {
		/*
		 * 0 error 1 scary 2 countdown 3 win 4 gasp 5 lose 6 cricket
		 */
		if (!volume.isChecked())
			soundArray.get(index).play(1.0f);
	}

	public ImageButton getVolume() {
		return volume;
	}

	public Music getMusic() {
		return music;
	}

	public void playMusic(int index) {
		if (!volume.isChecked()) {
			if (index == 0) {
				music.play();
				mainMenu = true;
			} else if (index == 1)
				winMusic.play();
		}
	}

	public void stopMusic(int index) {
		if (index == 0) {
			mainMenu = false;
			music.stop();
		} else
			winMusic.stop();
	}

	public AtlasRegion getAtlasRegion(int index) {
		/*
		 * 0 left fist 1 left fingers 2 left hand 3 left paper icon 4 left rock
		 * icon 5 left scissors icon 6 Cursor 7 right fingers 8 right fist 9
		 * right hand 10 volume off 11 volume on
		 */
		return arrayRegion.get(index);
	}

}
