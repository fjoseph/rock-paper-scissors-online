package com.Fawaz.RPS.Utils;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.delay;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.moveTo;

import java.text.DecimalFormat;
import java.util.Random;

import com.Fawaz.RPS.RPSOnline;
import com.Fawaz.RPS.Mob.RPSPlayer;
import com.Fawaz.RPS.Net.KryoClient;
import com.Fawaz.RPS.Net.Packets.PlayerInformation;
import com.Fawaz.RPS.Screens.MainMenu;
import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Timer;
import com.badlogic.gdx.utils.Timer.Task;

public class GameLogic {
	private Random rand;
	private boolean multiPlayer, disconnected = false, gameStarted = true;
	private int winRounds;
	private RPSOnline game;
	private Array<AtlasRegion> firstPlace, secondPlace;
	private RPSPlayer player, opponent;
	private Task checkInput, nextRound, hideHands;
	private Image rock, paper, scissors;
	private Stage stage;
	private Label exit, y, o, u, l, o2, s, e, w, i, n, status, opponentCard,
			playerCard;
	private float count;
	private countDownTask paperCountdown, scissorCountDown;
	private KryoClient client;
	private boolean countdown = false, sameHands = true, suspense = false;
	public DecimalFormat df;
	private Preferences pref = Gdx.app.getPreferences("RPSOnline");
	private int timesWon, consecutiveWon, rounds, sameHandsIndex = -1,
			modeCode, timesWon3, timesWon5, timesWon10, timesWon25,
			timesWon100;

	public GameLogic(int modeCode, int rounds, Stage sstage, RPSOnline g) {
		Gdx.app.setLogLevel(Application.LOG_DEBUG);

		// Parameter assigning
		multiPlayer = modeCode != 1;
		this.game = g;
		this.stage = sstage;
		this.modeCode = modeCode;
		this.rounds = rounds;

		player = new RPSPlayer(game);
		opponent = new RPSPlayer(game);
		rand = new Random();

		// Hands showing
		paperCountdown = new countDownTask(1);
		scissorCountDown = new countDownTask(2);

		// Timer format
		df = new DecimalFormat();

		// Hands for each side
		firstPlace = new Array<AtlasRegion>(3);
		secondPlace = new Array<AtlasRegion>(3);

		// Exit Button
		LabelStyle ls = new LabelStyle(game.getFont(), Color.BLACK);
		exit = new Label("Exit", ls);
		y = new Label("Y", ls);
		o = new Label("o", ls);
		u = new Label("u", ls);
		w = new Label("w", ls);
		i = new Label("i", ls);
		n = new Label("n", ls);
		l = new Label("l", ls);
		o2 = new Label("o", ls);
		s = new Label("s", ls);
		e = new Label("e", ls);
		status = new Label("", ls);
		playerCard = new Label("", ls);
		opponentCard = new Label("", ls);

		df.setMaximumFractionDigits(2);
		winRounds = rounds / 2 + 1;

		// getting the rps
		rock = game.getImage(0);
		paper = game.getImage(1);
		scissors = game.getImage(2);

		// setting listeners
		rock.addListener(new RPSChooser(0));
		paper.addListener(new RPSChooser(1));
		scissors.addListener(new RPSChooser(2));

		// Getting preference for achievements, leaderboard
		timesWon = pref.getInteger("timesWon", 0);
		timesWon3 = pref.getInteger("timesWon3", 0);
		timesWon5 = pref.getInteger("timesWon5", 0);
		timesWon10 = pref.getInteger("timesWon10", 0);
		timesWon25 = pref.getInteger("timesWon25", 0);
		timesWon100 = pref.getInteger("timesWon100", 0);
		consecutiveWon = pref.getInteger("consecutiveWon", 0);

		rock.setPosition(190, 0);
		paper.setPosition(280, 0);
		scissors.setPosition(360, 0);

		firstPlace.add(game.getAtlasRegion(0));
		firstPlace.add(game.getAtlasRegion(2));
		firstPlace.add(game.getAtlasRegion(1));
		secondPlace.add(game.getAtlasRegion(8));
		secondPlace.add(game.getAtlasRegion(9));
		secondPlace.add(game.getAtlasRegion(7));

		exit.setPosition(20, 15);
		exit.addListener(new InputListener() {
			@Override
			public boolean touchDown(InputEvent event, float x, float y,
					int pointer, int button) {
				// TODO Auto-generated method stub
				game.stopMusic(1);
				paperCountdown.cancel();
				scissorCountDown.cancel();
				checkInput.cancel();
				nextRound.cancel();
				hideHands.cancel();
				game.setScreen(new MainMenu(game));
				if (client != null)
					client.endConnection();
				if (gameStarted && !disconnected)
					game.showAd();

				return true;
			}
		});

		playerCard.setY(320);
		opponentCard.setY(320);
		playerCard.setText("Name:" + game.getName());

		checkInput = new Task() {

			@Override
			public void run() {
				if (player.handsIndex == -1) {
					player.handsIndex = rand.nextInt(3);
					Gdx.app.debug("CheckInput",
							"Player randomly chosen to be: "
									+ player.handsIndex);
				}

				if (opponent.handsIndex == -1) {
					opponent.handsIndex = rand.nextInt(3);
					Gdx.app.debug("CheckInput",
							"opponent randomly chosen to be: "
									+ opponent.handsIndex);
				}

				handsSuperiority();

			}
		};

		nextRound = new Task() {

			@Override
			public void run() {

				if (!gameOver()) {
					if (player.roundsWon == winRounds - 1
							&& opponent.roundsWon == winRounds - 1) {
						suspense = true;
						game.unlockAchievement(9);
						game.playSound(1);
					}
					startCountDown();
				} else {
					if (player.roundsWon == winRounds) {
						y.setPosition(265, 655);
						o.setPosition(280, -35);
						u.setPosition(295, 655);
						w.setPosition(325, -35);
						i.setPosition(347, 655);
						n.setPosition(355, -35);
						y.addAction(delay(.5f, moveTo(y.getX(), 320, .5f)));
						o.addAction(delay(1, moveTo(o.getX(), 320, .5f)));
						u.addAction(delay(1.5f, moveTo(u.getX(), 320, .5f)));
						w.addAction(delay(2, moveTo(w.getX(), 320, .5f)));
						i.addAction(delay(2.5f, moveTo(i.getX(), 320, .5f)));
						n.addAction(delay(3, moveTo(n.getX(), 320, .5f)));
						player.remove();
						opponent.remove();
						rock.remove();
						paper.remove();
						scissors.remove();
						stage.addActor(y);
						stage.addActor(o);
						stage.addActor(u);
						stage.addActor(w);
						stage.addActor(i);
						stage.addActor(n);
						game.playMusic(1);

						// achievements
						game.unlockAchievement(1); // win one game

						if (sameHands)
							game.unlockAchievement(3); // Same Hands;

						if (suspense) // Photo finish
							game.unlockAchievement(10);

						if (multiPlayer) {
							switch (GameLogic.this.rounds) {
							case 3:
								game.unlockAchievement(4);// Best of 3
								break;
							case 5:
								game.unlockAchievement(5);// Best of 5
								break;
							case 10:
								game.unlockAchievement(6);// Best of 10
								break;
							case 25:
								game.unlockAchievement(7);// Best of 25
								break;
							case 100:
								game.unlockAchievement(8);// Best of 100
								break;
							}

							timesWon++;
							consecutiveWon++;
							pref.putInteger("consecutiveWon", consecutiveWon);
							pref.putInteger("timesWon", timesWon);

							// Times won
							game.incrementWinCountAchievement();

							// consecutive wins
							switch (consecutiveWon) {
							case 2:
								game.unlockAchievement(11);
								break;
							case 3:
								game.unlockAchievement(12);
								break;
							case 4:
								game.unlockAchievement(13);
								break;
							case 5:
								game.unlockAchievement(14);
								break;
							}

							switch (GameLogic.this.rounds) {
							case 3:
								timesWon3++;
								pref.putInteger("timesWon3", timesWon3);
								game.submitScore(3, timesWon3);
								break;
							case 5:
								timesWon5++;
								pref.putInteger("timesWon5", timesWon5);
								game.submitScore(5, timesWon5);
								break;
							case 10:
								timesWon10++;
								pref.putInteger("timesWon10", timesWon10);
								game.submitScore(10, timesWon10);
								break;
							case 25:
								timesWon25++;
								pref.putInteger("timesWon25", timesWon25);
								game.submitScore(25, timesWon25);
								break;
							case 100:
								timesWon100++;
								pref.putInteger("timesWon100", timesWon100);
								game.submitScore(100, timesWon100);
								break;
							}

							pref.flush();

						}

					} else { // lose
						// achievements
						if (multiPlayer) {
							if (consecutiveWon == 4)
								game.unlockAchievement(15);
							consecutiveWon = 0;
							pref.putInteger("consecutiveWon", 0);
							pref.flush();
						}
						y.setPosition(rand.nextInt(640), rand.nextInt(640));
						o.setPosition(rand.nextInt(640), rand.nextInt(640));
						u.setPosition(rand.nextInt(640), rand.nextInt(640));
						l.setPosition(rand.nextInt(640), rand.nextInt(640));
						o2.setPosition(rand.nextInt(640), rand.nextInt(640));
						s.setPosition(rand.nextInt(640), rand.nextInt(640));
						e.setPosition(rand.nextInt(640), rand.nextInt(640));
						y.addAction(delay(rand.nextInt(3) + rand.nextFloat(),
								moveTo(265, 320, .5f)));
						o.addAction(delay(rand.nextInt(3) + rand.nextFloat(),
								moveTo(280, 320, .5f)));
						u.addAction(delay(rand.nextInt(3) + rand.nextFloat(),
								moveTo(295, 320, .5f)));
						l.addAction(delay(rand.nextInt(3) + rand.nextFloat(),
								moveTo(340, 320, .5f)));
						o2.addAction(delay(rand.nextInt(3) + rand.nextFloat(),
								moveTo(348, 320, .5f)));
						s.addAction(delay(rand.nextInt(3) + rand.nextFloat(),
								moveTo(364, 320, .5f)));
						e.addAction(delay(rand.nextInt(3) + rand.nextFloat(),
								moveTo(379, 320, .5f)));
						player.remove();
						opponent.remove();
						rock.remove();
						paper.remove();
						scissors.remove();
						stage.addActor(y);
						stage.addActor(o);
						stage.addActor(u);
						stage.addActor(l);
						stage.addActor(o2);
						stage.addActor(s);
						stage.addActor(e);
						game.playSound(5);
					}
				}

			}
		};

		hideHands = new Task() {

			@Override
			public void run() {
				rock.setVisible(false);
				paper.setVisible(false);
				scissors.setVisible(false);
			}
		};

		player.setRounds(rounds);
		opponent.setRounds(rounds);
		player.setName(game.getName());
		if (modeCode == 1) {
			player.setPlace(0, firstPlace);
			opponent.setPlace(1, secondPlace);
			opponent.setName("CPU");
			startCountDown();
			stage.addActor(player);
			stage.addActor(opponent);
		} else if (modeCode == 2) {
			status.setText("Looking for a room");
			client = new KryoClient(this, modeCode, rounds);
			status.setPosition(320 - (.5f * status.getPrefWidth()), 320);
			stage.addActor(status);
			gameStarted = false;

		} else if (modeCode == 3) {
			client = new KryoClient(this, modeCode, rounds);
			player.setPlace(0, firstPlace);
			opponent.setPlace(1, secondPlace);
			gameStarted = false;

		} else {
			client = new KryoClient(this, modeCode, rounds);
			opponent.setPlace(0, firstPlace);
			player.setPlace(1, secondPlace);
			gameStarted = false;

		}

		stage.addActor(rock);
		stage.addActor(paper);
		stage.addActor(scissors);
		stage.addActor(exit);
	}

	public void render(float delta) {
		if (countdown) {
			count -= delta;
			status.setText(df.format(count));
			status.setPosition(320 - (.5f * status.getPrefWidth()), 320);
			if (count < 0) {
				gameStarted = true;
				countdown = false;
				if (modeCode == 3)
					game.unlockAchievement(23);
				else if (modeCode != 1 && modeCode != 2)
					game.unlockAchievement(24);
				playerCard.remove();
				opponentCard.remove();
				status.remove();
				startCountDown();
				stage.addActor(player);
				stage.addActor(opponent);

			}
		}
		stage.act();
		stage.draw();
	}

	public boolean gameOver() {
		if (player.roundsWon == winRounds || opponent.roundsWon == winRounds) {
			if (multiPlayer) {
				client.gameOver();
				if (rounds == 100) {
					game.unlockAchievement(17);
				}
			}
			return true;
		} else
			return false;
	}

	public void startMultiplayerCountDown(float countdown) {
		count = countdown;
		this.countdown = true;
		stage.addActor(status);

	}

	public void startCountDown() {

		rock.setVisible(true);
		paper.setVisible(true);
		scissors.setVisible(true);

		player.changeHands(0);
		opponent.changeHands(0);
		opponent.handsIndex = -1;
		if (multiPlayer) {
			player.handsIndex = rand.nextInt(3);
			client.sendHand(player.handsIndex);
		} else
			player.handsIndex = -1;

		Timer.schedule(paperCountdown, 1);
		Timer.schedule(scissorCountDown, 2);
		Timer.schedule(hideHands, 2.5f);
		Timer.schedule(checkInput, 3);
		Timer.schedule(nextRound, 4);
	}

	public void handsSuperiority() {
		player.showHands();
		opponent.showHands();
		if (sameHandsIndex == -1)
			sameHandsIndex = player.handsIndex;
		else {
			if (sameHandsIndex != player.handsIndex) {
				sameHands = false;
			}
		}
		if (player.handsIndex == opponent.handsIndex) {
			game.playSound(6);
		} else if ((player.handsIndex == 0 && opponent.handsIndex == 2)
				|| (player.handsIndex == 1 && opponent.handsIndex == 0)
				|| (player.handsIndex == 2 && opponent.handsIndex == 1)) {
			player.roundsWon++;
			player.resetInfo();
			game.playSound(2);
		} else {
			opponent.roundsWon++;
			opponent.resetInfo();
			game.playSound(0);
		}
		// 1 player 1
		// 2 player 2
		// 3 no one won;
	}

	private class countDownTask extends Task {

		private int index;

		public countDownTask(int index) {
			this.index = index;
		}

		@Override
		public void run() {
			player.changeHands(index);
			opponent.changeHands(index);
			if (index == 2)
				game.playSound(4);
		}

	}

	public void setPlayerPlace(int place, boolean onlyOne) {
		if (place == 0) {
			player.setPlace(0, firstPlace);
			playerCard.setPosition(160 - (.5f * playerCard.getPrefWidth()),
					320 + (.5f * playerCard.getPrefHeight()));
		} else {
			player.setPlace(1, secondPlace);
			playerCard.setPosition(480 - (.5f * playerCard.getPrefWidth()),
					320 + (.5f * playerCard.getPrefHeight()));
		}
		if (onlyOne) {
			countdown = false;
			opponentCard.remove();
			status.remove();
		}
		stage.addActor(playerCard);
	}

	public void setOpponentPlace(int place, PlayerInformation pI) {
		opponent.setName(pI.name);
		opponentCard.setText("Name: " + pI.name);
		if (place == 0) {
			opponent.setPlace(0, firstPlace);
			opponentCard.setPosition(160 - (.5f * opponentCard.getPrefWidth()),
					320 + (.5f * opponentCard.getPrefHeight()));
		} else {
			opponent.setPlace(1, secondPlace);
			opponentCard.setPosition(480 - (.5f * opponentCard.getPrefWidth()),
					320 + (.5f * opponentCard.getPrefHeight()));
		}
		opponent.setName(pI.name);
		status.remove();
		stage.addActor(opponentCard);
	}

	public void setStatus(String message) {
		status.setText(message);
		status.setPosition(320 - (.5f * status.getPrefWidth()), 320);
		player.remove();
		opponent.remove();
		stage.addActor(status);

	}

	public void opponentDisconnect() {
		countdown = false;
		setStatus(opponent.getPlayerName() + " has disconnected");
		paperCountdown.cancel();
		scissorCountDown.cancel();
		checkInput.cancel();
		nextRound.cancel();
		disconnected = true;
	}

	public void changeOpponentsHands(int index) {
		System.out.println("Got: " + index);
		opponent.handsIndex = index;
	}

	public void setRounds(int rounds) {
		this.rounds = rounds;
		winRounds = rounds / 2 + 1;
		player.setRounds(rounds);
		opponent.setRounds(rounds);
		opponent.resetInfo();
		player.resetInfo();

	}

	public String getName() {
		return game.getName();
	}

	public class RPSChooser extends InputListener {
		private int index;

		public RPSChooser(int index) {
			this.index = index;
		}

		@Override
		public boolean touchDown(InputEvent event, float x, float y,
				int pointer, int button) {
			player.handsIndex = index;
			if (multiPlayer) {
				client.sendHand(index);
			}
			return true;
		}
	}

}
