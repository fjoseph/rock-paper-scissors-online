package com.Fawaz.RPS.Net;

import java.io.IOException;

import com.Fawaz.RPS.Net.Packets.DisconnectPacket;
import com.Fawaz.RPS.Net.Packets.HandsPacket;
import com.Fawaz.RPS.Net.Packets.LoginPacket;
import com.Fawaz.RPS.Net.Packets.LoginResponsePacket;
import com.Fawaz.RPS.Net.Packets.PlayerInformation;
import com.Fawaz.RPS.Net.Packets.PrivateRoomInformation;
import com.Fawaz.RPS.Net.Packets.RoomRequestPacket;
import com.Fawaz.RPS.Net.Packets.StartPacket;
import com.Fawaz.RPS.Utils.GameLogic;
import com.badlogic.gdx.Gdx;
import com.esotericsoftware.kryonet.Client;
import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;

public class KryoClient extends Listener {

	private Client client;
	private GameLogic gameLogic;
	private int version = 1;
	private int modeCode;
	private int rounds;

	public KryoClient(GameLogic gameLogic, int type, int rounds) {
		this.gameLogic = gameLogic;
		modeCode = type;
		this.rounds = rounds;
		client = new Client();

		client.getKryo().register(LoginPacket.class);
		client.getKryo().register(LoginResponsePacket.class);
		client.getKryo().register(RoomRequestPacket.class);
		client.getKryo().register(PlayerInformation.class);
		client.getKryo().register(PlayerInformation[].class);
		client.getKryo().register(StartPacket.class);
		client.getKryo().register(HandsPacket.class);
		client.getKryo().register(DisconnectPacket.class);
		client.getKryo().register(PrivateRoomInformation.class);

		client.addListener(this);
		client.start();
		try {
			// client.connect(2000, "192.168.8.107", 55556);
			client.connect(2000, "23.239.17.61", 55556);
		} catch (IOException e) {
			gameLogic
					.setStatus("Server not reachable\n Check your Internet Connection");
			e.printStackTrace();
		}

		LoginPacket lp = new LoginPacket();
		lp.version = version;

		client.sendTCP(lp);
	}

	@Override
	public void connected(Connection connection) {
		connection.setTimeout(7000);
		connection.setKeepAliveTCP(2000);
	}

	@Override
	public void disconnected(Connection connection) {
	}

	@Override
	public void received(Connection connection, Object object) {
		if (!object.getClass().getSimpleName().equals("KeepAlive"))
			Gdx.app.debug("Recieved", object.getClass().getSimpleName());
		if (object instanceof LoginResponsePacket) {
			LoginResponsePacket lrp = (LoginResponsePacket) object;
			if (lrp.accepted) {
				RoomRequestPacket rrp = new RoomRequestPacket();
				rrp.requestType = modeCode;
				rrp.name = gameLogic.getName();
				rrp.rounds = rounds;
				client.sendTCP(rrp);
			} else {
				gameLogic.setStatus(lrp.message);
			}
		} else if (object instanceof PlayerInformation[]) {
			PlayerInformation[] roomInformation = (PlayerInformation[]) object;
			int i = 0;
			for (PlayerInformation pI : roomInformation) {
				if (pI.id == connection.getID()) {
					gameLogic.setPlayerPlace(i, roomInformation.length == 1);
				} else {
					gameLogic.setOpponentPlace(i, pI);
				}
				i++;
			}
		} else if (object instanceof StartPacket) {
			StartPacket sp = (StartPacket) object;
			gameLogic.startMultiplayerCountDown(sp.startTime);
		} else if (object instanceof DisconnectPacket) {
			gameLogic.opponentDisconnect();
		} else if (object instanceof HandsPacket) {
			HandsPacket hp = (HandsPacket) object;
			gameLogic.changeOpponentsHands(hp.handsIndex);
		} else if (object instanceof PrivateRoomInformation) {
			PrivateRoomInformation pri = (PrivateRoomInformation) object;
			gameLogic.setStatus("Id: " + pri.id);
			gameLogic.setRounds(pri.rounds);
		}
	}

	public void sendHand(int handsIndex) {
		HandsPacket hp = new HandsPacket();
		hp.handsIndex = handsIndex;
		System.out.println("sending: " + handsIndex);
		client.sendTCP(hp);

	}

	public void endConnection() {
		client.close();
	}

	public void gameOver() {
		DisconnectPacket dp = new DisconnectPacket();
		client.sendTCP(dp);
	}

}
