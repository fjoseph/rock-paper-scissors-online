package com.Fawaz.RPS;

import com.Fawaz.RPS.Screens.SplashScreen;
import com.Fawaz.RPS.Utils.ResourceFactory;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;

public class RPSOnline extends Game {

	private ResourceFactory resource;
	private ThirdPartyInteface googlePlay;

	public RPSOnline(ThirdPartyInteface googlePlay) {
		this.googlePlay = googlePlay;
	}

	@Override
	public void create() {
		resource = new ResourceFactory();
		googlePlay.Login();
		setScreen(new SplashScreen(this));
	}

	@Override
	public void dispose() {
		super.dispose();
		getScreen().dispose();
		resource.dispose();
	}

	public Image getImage(int index) {

		return resource.getImage(index);
	}

	public AtlasRegion getAtlasRegion(int index) {
		/*
		 * 0 left fist 1 left fingers 2 left hand 3 left paper icon 4 left rock
		 * icon 5 left scissors icon 6 Cursor 7 right fingers 8 right fist 9
		 * right hand 10 volume off 11 volume on
		 */
		return resource.getAtlasRegion(index);
	}

	public BitmapFont getFont() {
		return resource.getFont();
	}

	public void getSound(int index) {
		/*
		 * 0 error 1 scary 2 win 3 beep 4 gasp 5 lose 6 cricket
		 */
		resource.playSound(index);
	}

	public void playMusic(int index) {
		resource.playMusic(index);
	}

	public void stopMusic(int index) {
		resource.stopMusic(index);
	}

	public ImageButton getVolume() {
		return resource.getVolume();
	}

	public void playSound(int index) {
		resource.playSound(index);
	}

	public void Login() {
		googlePlay.Login();
	}

	public String getName() {
		return googlePlay.getName();
	}

	public void unlockAchievement(int index) {
		googlePlay.unlockAchievements(index);
	}

	public void submitScore(int rounds, int score) {
		googlePlay.submitScore(rounds, score);
	}

	public void displayLeaderBoard() {
		googlePlay.showLeaderBoards();
	}

	public void displayAchievements() {
		googlePlay.displayAchievements();
	}

	public boolean isSignIn() {
		return googlePlay.getSignedIn();
	}

	public void showAd() {
		resource.stopMusic(0);
		resource.stopMusic(1);
		googlePlay.showAd();
	}

	public void incrementWinCountAchievement() {
		googlePlay.incrementWinCountAchievement();
	}
}
