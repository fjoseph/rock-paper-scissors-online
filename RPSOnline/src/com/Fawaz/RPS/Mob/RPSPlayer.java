package com.Fawaz.RPS.Mob;

import com.Fawaz.RPS.RPSOnline;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Array;

public class RPSPlayer extends Group {
	private Image hands;
	private Label info;
	public int roundsWon;
	public int handsIndex,rounds;
	private String name;
	private Array<AtlasRegion> handsTexture;
	private TextureRegionDrawable trd;
	private RPSOnline game;

	public RPSPlayer(RPSOnline game) {
		trd = new TextureRegionDrawable();
		hands = new Image(trd);
		this.game = game;
		roundsWon = 0;
		handsIndex = -1;
		LabelStyle ls = new LabelStyle(game.getFont(), Color.BLACK);
		info = new Label("", ls);
		info.setPosition(0, 515);
		setSize(320, 540);
		hands.setBounds(0, 0, 320, 515);
		addActor(hands);
		addActor(info);

	}

	public void setPlace(int i, Array<AtlasRegion> handsTexture) {
		this.handsTexture = handsTexture;
		if (i == 0)
			setPosition(0, 100);
		else
			setPosition(320, 100);
	}

	public void showHands() {
		Gdx.app.debug("HandsIndex", handsIndex + "");
		trd.setRegion(handsTexture.get(handsIndex));
	}

	public void setName(String name) {
		this.name = name;
		info.setText(name + " " + roundsWon + " out of " + rounds);
	}

	public void setRounds(int rounds) {
		this.rounds = rounds;
	}

	public void resetInfo() {
		info.setText(name + " " + roundsWon + " out of " + rounds);
	}

	public String getPlayerName() {
		return name;
	}

	public void changeHands(int i) {
		trd.setRegion(handsTexture.get(i));
		game.playSound(3);
	}

}
